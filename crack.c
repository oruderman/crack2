#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *h = md5(guess, strlen(guess));
    
    // Compare the two hashes
    if (strcmp(hash, h) == 0) 
    {
        return 1;
    }
    return 0;
    
    // Free any malloc'd memory
    free(h);
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    FILE *f = fopen(filename, "r");
    if(!f)
    {
        printf("Couldn't open %s.\n", filename);
        exit(1);
    }
    int size = 0;
    char **m = (char **)malloc(size*sizeof(char*));
    char temp[100];
    int i = 0;
    while(fscanf(f, "%s", temp) != EOF)
    {
        if(i == size)
        {
            size +=100;
            char** newm = (char**)realloc(m, size*sizeof(char*));
            if(newm == NULL)
            {
                puts("Couldn't extend the array.\n");
                exit(1);
            }
            else
                m = newm;
        }
        int len = strlen(temp);
        m[i] = (char *)malloc((len+1)*sizeof(char*));
        strcpy(m[i], temp);
        i++;
    }
    fclose(f);
    m[i]=NULL;
    return m;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);
    
    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]); 
    
    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    for ( int i = 0; hashes[i] != NULL ; i++)
    {
        for ( int j = 0; dict[j] != NULL; j++)
        {
            if ( tryguess(hashes[i], dict[j]))
            {
                printf("%s:   %s\n", hashes[i], dict[j]);
            }
        }
    }
} 
 
 